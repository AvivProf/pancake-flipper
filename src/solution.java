import java.io.*;
import java.util.*;

public class solution {


    static int[] pancakeSort(int[] arr) {
        int lastValue = 0;
        while (!checkSorted(arr)) {
            if (checkSorted(flip(arr, arr.length))) {
                System.out.println("It was finished upside down, so flipping!");
                return flip(arr, arr.length);
            }
            for (int i = 0; i < arr.length; i++) {
                if (i != 0) {
                    if (checkSorted(arr)) {
                        return arr;
                    } else if (i == arr.length - 1 && !checkSorted(arr)) {
                        arr = flip(arr, i + 1);
                        printTheFlip(arr);
                    } else if ((arr[i] == lastValue + 1) && (arr[i + 1] < arr[i])) {
                        arr = flip(arr, i + 1);
                        printTheFlip(arr);
                        i = 0;
                        break;
                    } else if (arr[i] == lastValue - 1 && arr[i + 1] > arr[i]) {
                        arr = flip(arr, i + 1);
                        printTheFlip(arr);
                    }
                }
                lastValue = arr[i];
            }
        }
        if (checkSorted(arr)) {
            return arr;
        } else if (checkSorted(flip(arr, arr.length))) {
            arr = flip(arr, arr.length);
            printTheFlip(arr);
        }
        return arr;
    }


    public static boolean checkSorted(int[] arr) {
        int lastChecked = arr[0];
        for (int value : arr) {
            if (lastChecked > value) {
                return false;
            } else {
                lastChecked = value;
            }
        }
        return true;

    }


    public static int[] flip(int[] arr, int k) {
        int[] flipped = new int[arr.length];
        int j = k - 1;
        for (int i = 0; i < arr.length; i++) {
            if (i < k) {
                flipped[i] = arr[j];
                j--;
            } else {
                flipped[i] = arr[i];
            }
        }
        return flipped;
    }


    public static void printTheFlip(int[] arr) {
        System.out.println("flipped! " + Arrays.toString(arr));
    }


    public static void main(String[] args) {
        int[] array = {4, 5, 2, 3, 1, 6};
        System.out.println(Arrays.toString(array));

        int[] sortedArray = pancakeSort(array);
        System.out.println(Arrays.toString(sortedArray));
    }
}
